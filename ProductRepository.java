package com.example.springbootcrudexample;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
   Product findByName(String name);
   
}




