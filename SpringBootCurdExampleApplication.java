package com.example.springbootcrudexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class })
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class SpringBootCurdExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCurdExampleApplication.class, args);
	}

}
//https://www.javaguides.net/2021/12/how-to-use-spring-data-jpa-in-spring-boot-project.html